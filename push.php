#!/usr/bin/env php
<?php

/**
 * @todo
 * - bug with Tags. And they become messy in issue creation UI.
 */

use Gitlab\Client;
use Gitlab\Model\Project;
use GuzzleHttp\RequestOptions;

require 'vendor/autoload.php';

$slug = $argv[1];

$guzzle = new \GuzzleHttp\Client(['base_uri' => 'https://www.drupal.org']);

// Uses personal token with grant 'api' scope: https://gitlab.com/profile/personal_access_tokens
if (!$token = getenv('GITLAB_TOKEN')) {
    exit('Missing token');
}
$client = Client::create(getenv('GITLAB_URL') ?: 'https://gitlab.com')->authenticate($token, Client::AUTH_OAUTH_TOKEN);
$path_with_namespace = "drupalspoons/$slug";
if (!$project_array = $client->projects()->show($path_with_namespace)) {
  throw new Exception('Could not find '. $path_with_namespace . ' on Gitlab');
}
$project = new Project($project_array['id'], $client);

// Get an issue to see what Gitlab data looks like.
// $issue = $project->issue(44);

// Get all source data
$project_nid = get_project_nid($slug, $guzzle);
$issues = pull_issues($project_nid, $guzzle);
if (empty($issues)) {
  logg('No issues found.');
  exit(0);
}
$comments = pull_comments($issues, $guzzle);
$users = pull_users($issues, $comments, $guzzle);
$terms = pull_terms($issues, $guzzle);

logg("Found " . count($issues) . " issues.");
logg("Found " . count($comments) . " comments.");
logg("Found " . count($terms) . " terms.");
logg("Found " . count($users) . " users.");
logg("Target project is $slug");

$issue_map = create_issues($issues, $terms, $comments, $users, $project);
create_related($issues, $issue_map);

/**
 * @param array $issues
 * @param array $terms
 * @param array $comments
 * @param array $users
 * @param \Gitlab\Model\Project $project
 *
 * @return \Gitlab\Model\Issue[]
 */
function create_issues($issues, array $terms, $comments, $users, Project $project) {
  $issue_map = [];
  foreach ($issues as $record) {
    $params = [
      'description' => get_description($record, $users),
      'labels' => get_labels($record, $terms),
      'closed' => FALSE, // Only migrating open issues.
      'updated_at' => date(DateTimeInterface::ISO8601, $record->changed),
      'created_at' => date(DateTimeInterface::ISO8601, $record->created),
      'weight' => $record->nid,
    ];
    $issue = $project->createIssue($record->title, $params);
    logg('Created issue '. $issue->iid);
    // sleep(1);
    $issue_map[$record->nid] = $issue;

    create_comment_thread($comments, $users, $record, $issue);
  }
  return $issue_map;
}

/**
 * Create the comments associated with an issue.
 *
 * @param $comments
 * @param $users
 * @param $record
 * @param \Gitlab\Model\Issue $issue
 */
function create_comment_thread($comments, $users, $record, \Gitlab\Model\Issue $issue): void {
  // Now, create the comments for this node.
  foreach ($record->comments as $comment) {
    $body = $comments[$comment->id]->comment_body ? $comments[$comment->id]->comment_body->value : '';
    $body = \Drupal\Component\Utility\Html::transformRootRelativeUrlsToAbsolute($body, 'https://www.drupal.org');
    $name = $users[$comments[$comment->id]->author->id];
    if (empty($body) || strpos($body, 'created an issue.') || $name == 'System Message') {
      // Skip silly 1st comment thats pointless AFAICT.
      continue;
    }

    // Prepend unmigrated data.
    $preamble = sprintf("> [Comment #%s](%s) by [%s](https://www.drupal.org/user/%s). \n\n",
      $comment->id,
      $comments[$comment->id]->url,
      $name,
      $comments[$comment->id]->author->id
    );
    $note = $issue->addComment($preamble . "\n\n" . $body, date(DateTimeInterface::ISO8601, $comments[$comment->id]->created));
    logg('Created note ' . $note->id);
  }
}

/**
 * @param array $issues
 * @param \Gitlab\Model\Issue[] $issue_map
 */
function create_related($issues, $issue_map) {
  foreach ($issues as $issue) {
    foreach ($issue->field_issue_related as $related) {
      if (isset($issue_map[$related->id])) {
        // OK, we know about the related issue.
        $gitlab = $issue_map[$issue->nid];
        $gitlab->addLink($issue_map[$related->id]);
        logg('Created related issue on '. $gitlab->iid);
      }
    }
    if (isset($issue->field_issue_parent) && isset($issue_map[$issue->field_issue_parent->id])) {
      // OK, we know about the parent issue.
      $gitlab = $issue_map[$issue->nid];
      $gitlab->addLink($issue_map[$issue->field_issue_parent->id]);
      logg('Created parent issue on '. $gitlab->iid);
    }
  }
}

function get_project_nid($slug, $guzzle) {
  $response = $guzzle->get("api-d7/node.json?field_project_machine_name=$slug");
  $project = json_decode($response->getBody())->list[0];
  return $project->nid;
}

function pull_issues($source, $guzzle) {
  $all = [];
  // Sadly, d.o. cannot OR a set of field values. So fetch separately.
  $statuses = [1,2,4,8,13,14,15,16];
  foreach ($statuses as $status) {
    $params = [
      'type' => 'project_issue',
      'field_project' => $source,
      'field_issue_status' => $status,
      'sort' => 'changed',
      'direction' => 'ASC',
    ];
    $new = (object) [
      'self' => 0,
      'last' => 999, // Just initializing to anything other than 0.
      'next' => 'api-d7/node?' . http_build_query($params),
    ];

    while (isset($new->next)) {
      $response = $guzzle->get($new->next, [RequestOptions::HEADERS => ['Accept' => 'application/json']]);
      $new = json_decode($response->getBody());
      $all = array_merge($all, $new->list);
    }
  }
  return $all;
}

function pull_comments($issues, $guzzle) {
  $cids = $params = $all = $comments = [];
  foreach ($issues as $issue) {
    $cids = array_merge($cids, array_column($issue->comments, 'id'));
  }
  foreach (array_chunk($cids, 50) as $chunk) {
    foreach ($chunk as $id) {
      $params[$id] = "cid[]=$id";
    }
    $response = $guzzle->get("api-d7/comment.json?" . implode('&', $params));
    $new = json_decode($response->getBody())->list;
    $all = array_merge($all, $new);
    unset($params);
  }

  // Index by cid.
  foreach ($all as $obj) {
    $comments[$obj->cid] = $obj;
  }
  return $comments;
}

function pull_users($issues, $comments, $guzzle) {
  $uids = $params = $all = $users = [];
  foreach ($issues as $issue) {
    $uids = array_merge($uids, [$issue->author->id]); // $issue->field_issue_assigned->id is missing
  }
  foreach ($comments as $comment) {
    $uids[] = $comment->author->id;
  }
  foreach (array_chunk(array_unique(array_filter($uids)), 50) as $chunk) {
    foreach ($chunk as $uid) {
      $params[$uid] = "uid[]=$uid";
    }
    $response = $guzzle->get("api-d7/user.json?" . implode('&', $params));
    $new = json_decode($response->getBody())->list;
    $all = array_merge($all, $new);
    unset($params);
  }

  // Index by uid.
  foreach ($all as $obj) {
    $users[$obj->uid] = $obj->name;
  }
  return $users;
}

function pull_terms($issues, $guzzle) {
  $tids = $all = $terms = $params = [];

  // Get tids.
  foreach ($issues as $issue) {
    if ($taxo = $issue->taxonomy_vocabulary_9) {
      $tids = array_merge($tids, array_column($taxo, 'id'));
    }
  }
  // API only returns 50 terms at a time AFAICT.
  foreach (array_chunk(array_unique($tids), 50) as $chunk) {
    foreach ($chunk as $tid) {
      $params[$tid] = "tid[]=$tid";
    }
    $response = $guzzle->get('api-d7/taxonomy_term.json?vocabulary=9&' . implode('&', $params));
    $new = json_decode($response->getBody())->list;
    $all = array_merge($all, $new);
    unset($params);
  }

  // Index by tid.
  foreach ($all as $obj) {
    $terms[$obj->tid] = $obj->name;
  }
  return $terms;
}

function get_description($record, $users) {
  $description = $record->body->value;

  // Prepend unmigrated data.
  $preamble = sprintf("> [Issue #%s](%s) on drupal.org by [%s](https://www.drupal.org/user/%s). \n\n",
    $record->nid,
    $record->url,
    $record->author->name,
    $record->author->id
  );

//  if (isset($record->field_issue_assigned)) {
//    $preamble .= 'Assigned to ' . sprintf("[%s](https://www.drupal.org/user/%s)", key($users[$record->field_issue_assigned->id]), $record->field_issue_assigned->id);
//  }

  return $preamble . \Drupal\Component\Utility\Html::transformRootRelativeUrlsToAbsolute($description, 'https://www.drupal.org');
}

/**
 * @param $record
 *
 * @param $terms
 *
 * @return String[]
 */
function get_labels($record, $terms) {
  $labels[] = get_component($record);
  $labels[] = get_priority($record);
  $labels[] = get_category($record);
  $labels[] = get_state($record);
  $labels[] = get_version($record);
  $labels[] = get_why($record);
  $labels = array_merge($labels, get_tags($record, $terms));
  return array_filter($labels);
}

function get_version($record) {
  if ($val = $record->field_issue_version) {
    return 'version::' . $val;
  }
}

function get_component($record) {
  if ($val = $record->field_issue_component) {
    return 'component::' . camelize($val);
  }
}

function get_tags($record, $terms) {
  $labels = [];
  foreach ($record->taxonomy_vocabulary_9 as $obj) {
    // $name = $terms[$obj->id];
    // Skip. Causing all labels to get dropped. Also adding clutter.
    // $labels[] = "$name";
  }
  return $labels;
}

function get_priority($record) {
    $map = [
      400 => 'critical',
      300 => 'major',
      200 => 'normal',
      100 => 'minor',
    ];
    if ($val = $map[$record->field_issue_priority]) {
      return 'priority::' . $val;
    }
}

function get_category($record) {
    $map = [
      1 => 'bug',
      3 => 'feature',
      5 => 'plan',
      4 => 'support',
      2 => 'task',
    ];
    if ($val = $map[$record->field_issue_category]) {
      return 'category::' . $val;
    }
}

function get_why($record) {
    $map = [
      3 => 'duplicate',
      5 => 'wontFix',
      6 => 'worksAsDesigned',
      16  => 'needsInfo',
      17  => 'outdated',
      18 => 'cannotReproduce',
    ];
    if ($val = @$map[$record->field_issue_status]) {
      return 'why::' . $val;
    }
}

function get_state($record) {
  $map = [
    1 => 'accepted',
    2 => 'fixed',
    3 => 'closed',
    4 => 'postponed',
    5 => 'closed',
    6 => 'closed',
    7 => 'closed',
    8 => 'needsReview',
    13 => 'needsWork',
    14 => 'rtbc',
    15 => 'toBePorted',
    16  => 'postponed',
    17  => 'closed',
    18 => 'closed',
  ];
  if ($val = $map[$record->field_issue_status]) {
    return 'state::' . $val;
  }
}

function logg(string $msg) {
  return fwrite(STDERR,$msg . "\n");
}

  /**
   * Camelize a string.
   *
   * @param $string
   * @param bool $upper_camel
   *
   * @return string|string[]
   */
  function camelize($string, $upper_camel = FALSE) {
    $output = preg_replace('/([^A-Z])([A-Z])/', '$1 $2', $string);
    $output = strtolower($output);
    $output = preg_replace('/[^a-z0-9]/', ' ', $output);
    $output = trim($output);
    $output = ucwords($output);
    $output = str_replace(' ', '', $output);
    return $upper_camel ? $output : lcfirst($output);
  }

